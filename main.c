#include <stdio.h>
#include <stdlib.h>
#pragma warning(disable : 4996)
#pragma warning(disable : 4703)
#include "tp4.h"
#include <string.h>

int main()
{
    /// Definition des variables globales ///
    //T_ListeVaccins* VACCIN = NULL;
    //T_ListeVaccins** ptVacc = malloc(sizeof(T_ListeVaccins));
    //ptVacc = &VACCIN;
    T_ABR* ARBRE = NULL;
    T_ABR** ptAbr;
    unsigned int nb;
    int initia = 0;

    char choix = '0';
    while (choix != '6') {
        printf("\n======================================");
        printf("\n1. Initialiser un ABR");
        printf("\n2. Ajouter un nombre de vaccins d'une marque dans un ABR par une date indiquée.");
        printf("\n3. Afficher tous les stocks disponibles dans un ABR.");
        printf("\n4. Compter le nombre de vaccins disponible pour une marque indiquée.");
        printf("\n5. Déduire un nombre de vaccins d'une marque indiquée dans un ABR.");
        printf("\n6. Quitter");
        printf("\n======================================");
        printf("\n   Votre choix ? ");
        choix = getchar();
        viderBuffer();

        switch (choix) {
            case '1':
                printf("\n ==========Vous souhaitez initialiser un ABR===========\n");
                ptAbr = malloc(sizeof(T_ABR)); //Allocation de la place dans la mémoire
                if (ptAbr == NULL) { //Affichages si l'allocation échoue
                    printf("    Initialisation échouée");
                    printf("\n    Exit\n");
                    break;
                }
                ptAbr = &ARBRE;
                initia = 1;
                printf("Vous venez d'initialiser un ABR.");
                break;
            case '2':
                /*date = "2020-05-15";
                marque = "Moderna";
                int nb = 20;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-10";
                marque = "Moderna";
                nb = 15;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-15";
                marque = "Pfizer";
                nb = 25;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-25";
                marque = "Pfizer";
                nb = 30;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-15";
                marque = "Pfizer";
                nb = 10;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-12";
                marque = "Moderna";
                nb = 15;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-20";
                marque = "Pfizer";
                nb = 10;
                ajouterVaccinA(ptAbr, date, marque, nb);

                date = "2020-05-20";
                marque = "Moderna";
                nb = 15;
                ajouterVaccinA(ptAbr, date, marque, nb);*/

                printf("\n ==========Vous souhaitez ajouter un stock===========\n");

                //Verification de l'initialisation
                if (initia == 0) {
                    printf("    Attention l'arbre n'est pas initialisé ");
                    printf("\n    Exit\n");
                    break;
                }

                //Choix de la date
                char *date = (char *) malloc(10 * sizeof(char)); //Allocation de la place dans la mémoire
                if (date == NULL) { //Affichage si l'allocation échoue
                    printf("    Initialisation de la date échouée");
                    printf("\n    Exit\n");
                    break;
                }
                printf("\n Entrez la date pour laquelle il faut ajouter un stock ou -1 pour sortir : ");
                scanf("%s", date);
                if ((strcmp(date, "-1")) == 0) {
                    viderBuffer();
                    printf("\n    Exit\n");
                    break;
                }
                viderBuffer();

                //Choix marque
                char *marque = (char *) malloc(10 * sizeof(char)); //Allocation de la place dans la mémoire
                if (marque == NULL) { //Affichage si l'allocation échoue
                    printf("    Initialisation de la marque échouée");
                    printf("\n    Exit\n");
                    break;
                }
                printf("\n Entrez la marque du vaccin ou -1 pour sortir : ");
                scanf("%s", marque);
                if ((strcmp(marque, "-1")) == 0) {
                    viderBuffer();
                    printf("\nExit\n");
                    break;
                }
                viderBuffer();

                //Choix du nombre de vaccins
                printf("\n Entrez le nombre de vaccins à ajouter ou -1 pour sortir : ");
                scanf("%d", &nb);
                if (nb == -1) {
                    viderBuffer();
                    printf("\n   Exit\n");
                    break;
                }
                if (nb < 0) {
                    printf("\n   ERREUR : le nombre de vaccins doit être positif");
                    printf("\n   Exit\n");
                    viderBuffer();
                    break;
                }
                viderBuffer();

                //Insertion
                ajouterVaccinA(ptAbr, date, marque, nb);
                break;

            case '3':
                printf("\n ==========Vous souhaitez afficher tous les stock disponibles===========\n");

                //Verification de l'initialisation
                if (initia == 0) {
                    printf("    Attention l'arbre n'est pas initialisé ");
                    printf("\n    Exit\n");
                    break;
                }

                //Verification que l'arbre n'est pas vide
                if (ARBRE == NULL) {
                    printf("L'arbre est vide");
                    break;
                }

                //Affichage
                afficherStockA(ARBRE);
                break;

            case '4':
                printf("\n ==========Vous souhaitez compter le nombre de vaccin d'une marque===========\n");

                //Verification de l'initialisation
                if (initia == 0) {
                    printf("    Attention l'arbre n'est pas initialisé ");
                    printf("\n    Exit\n");
                    break;
                }

                //Verification que l'arbre n'est pas vide
                if (ARBRE == NULL) {
                    printf("L'arbre est vide");
                    break;
                }

                //Choix de la marque
                char *nom_vaccin = (char *) malloc(10 * sizeof(char));
                if (nom_vaccin == NULL) { //Affichage si l'allocation échoue
                    printf("    Initialisation de la marque échouée");
                    printf("\n    Exit\n");
                    break;
                }
                printf("\n   Entrez la marque du vaccin ou -1 pour sortir : ");
                scanf("%s", nom_vaccin);
                if ((strcmp(nom_vaccin, "-1")) == 0) {
                    viderBuffer();
                    printf("\nExit\n");
                    break;
                }
                viderBuffer();

                //Comptage des vaccins
                int nbV = compterVaccins(ARBRE, nom_vaccin);
                printf("Il y a %d vaccins de la marque %s", nbV, nom_vaccin);
                break;

            case '5':
                printf("\n ==========Vous souhaitez deduire un nombre de vaccin d'une marque===========\n");

                //Verification de l'initialisation
                if (initia == 0) {
                    printf("    Attention l'arbre n'est pas initialisé ");
                    printf("\n   Exit\n");
                    break;
                }

                //Verification que l'arbre n'est pas vide
                if (ARBRE == NULL) {
                    printf("L'arbre est vide");
                    break;
                }

                //Entrée du nom de la marque
                printf("\t Entrez la marque du vaccin ou -1 pour sortir : ");
                char *nom_marque = (char *) malloc(10 * sizeof(char));
                scanf("%s", nom_marque);
                //Varifications du choix
                if ((strcmp(nom_marque, "-1")) == 0) {
                    viderBuffer();
                    printf("\nExit\n");
                    break;
                }
                viderBuffer();

                //Recherche du nombre de vaccins pour cette marque et affichage
                int nV = compterVaccins(ARBRE, nom_marque);
                if (nV == 0){
                    printf("\n Il n'y a aucun vaccin pour cette marque on ne peut pas en enlever");
                    printf("\nExit\n");
                    break;
                }
                printf("\t Il y a atcuellement %d vaccins pour la marque %s \n", nV ,nom_marque);

                //Entrée du nombre de vaccins
                printf("\t Entrez le nombre de vaccin à deduire ou -1 pour sortir :");
                scanf("%d", &nb);
                //Varifications du choix
                if (nb == -1) {
                    printf("\nExit\n");
                    viderBuffer();
                    break;
                }
                if (nb < 0) {
                    printf("\n   ERREUR : le nombre de vaccins doit être positif ");
                    printf("\n   Exit\n");
                    viderBuffer();
                    break;
                }
                if (nb > nV) {
                    printf("\n   ERREUR : le nombre de vaccins entré est superieur au nombre de vaccins actuel pour cette marque ");
                    printf("\n   Exit\n");
                    viderBuffer();
                    break;
                }
                viderBuffer();

                //Deduction
                deduireVaccinA(ptAbr, nom_marque, nb);
                printf("\t Les vaccins ont bien été déduit ");
                break;

            case '6':
                //desalocation de la mémoire 
                free_abr(ARBRE);
                printf("\n======== PROGRAMME TERMINE ========\n");
                break;

            default:
                printf("\n\nERREUR : votre choix n'est valide ! ");

        }
        printf("\n\n\n");

    }
    /*T_ListeVaccins* VACCIN = NULL;
    T_ListeVaccins** ptVacc = malloc(sizeof(T_ListeVaccins));
    ptVacc = &VACCIN;*/
    //----TESTS----//
    /*char* marque = "Moderna";
    int nb = 3;
    ajouterVaccinL(ptVacc, marque, nb);
    marque = "Pfizer";
    nb = 20;
    ajouterVaccinL(ptVacc, marque, nb);
    marque = "Moderna";
    nb = 30;
    ajouterVaccinL(ptVacc, marque, nb);
    marque = "Moderna";
    nb = 15;
    ajouterVaccinL(ptVacc, marque, nb);
    marque = "Pfizer";
    nb = 15;
    ajouterVaccinL(ptVacc, marque, nb);
    afficherStockL(VACCIN);

    ajouterVaccinL(ptVacc, marque, nb);
    marque = "AstraZeneca";
    nb = 15;
    ajouterVaccinL(ptVacc, marque, nb);
    afficherStockL(VACCIN);

    printf("\n");
    deduireVaccinL(ptVacc, "Moderna", 48);
    afficherStockL(VACCIN);

    printf("\n");
    deduireVaccinL(ptVacc, "AstraZeneca", 8);
    afficherStockL(VACCIN);*/

    /*printf("\n");
    deduireVaccinL(ptVacc, "AstraZeneca", 8);
    afficherStockL(VACCIN);*/


    //T_ABR* ARBRE = NULL;
    //T_ABR * *ptAbr = malloc(sizeof(T_ABR));
    //ptAbr = &ARBRE;
    //

    /*char* date = "2020-05-15";
    char* marque = "Moderna";
    int nb = 20;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-10";
    marque = "Moderna";
    nb = 15;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-15";
    marque = "Pizer";
    nb = 25;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-25";
    marque = "Pizer";
    nb = 30;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-15";
    marque = "Pizer";
    nb = 10;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-12";
    marque = "Moderna";
    nb = 15;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-20";
    marque = "Pizer";
    nb = 10;
    ajouterVaccinA(ptAbr, date, marque, nb);

    date = "2020-05-20";
    marque = "Moderna";
    nb = 15;
    ajouterVaccinA(ptAbr, date, marque, nb);*/

    //afficherStockA(ARBRE);
    //printf("Il y a %d vaccins de la marque %s\n",compterVaccins(ARBRE, marque), marque);

    //supprimer_noeud(ARBRE, "2020-05-10");
    //deduireVaccinA(ptAbr, "Pizer", 40);

    /*printf("\n");
    supprimer_noeud(ptAbr,"2020-05-12");
    supprimer_noeud(ptAbr,"2020-05-25");
    afficherStockA(ARBRE);*/

    //t_vaccin_elt* GESTION_VACCINS[10] = { NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL };



    return 0;
}
