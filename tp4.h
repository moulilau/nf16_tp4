//#ifndef Header_H_INCLUDED
//#define Header_H_INCLUDED


/* ========== STRUCTURES ========== */
// TODO : d�finissez vos structures ici
typedef struct ListeVaccins {
    char* marque;
    unsigned int nombre_vaccins;
    struct ListeVaccins* suivant;
} T_ListeVaccins;

typedef struct ABR {
    char* date;
    T_ListeVaccins* liste_vaccins;
    struct ABR* fils_gauche;
    struct ABR* fils_droite;
} T_ABR;



/* ========== FONCTIONS ========== */
void ajouterVaccinL(T_ListeVaccins** listeVaccins, char* marque, int nb_vaccins);
void ajouterVaccinA(T_ABR** abr, char* date, char* marque, int nb_vaccins);
void afficherStockL(T_ListeVaccins* listeVaccins);
void afficherStockA(T_ABR* abr);
int compterVaccins(T_ABR* abr, char* marque);
void deduireVaccinL(T_ListeVaccins** listeVaccins, char* marque, int nb_vaccins);
void deduireVaccinA(T_ABR** abr, char* marque, int nb_vaccins);

/* ------- UTILITAIRES ------------ */
void viderBuffer();
T_ABR* creerElement(char* date);
T_ABR* supprimer_noeud(T_ABR** abr, char* date);
T_ABR* successeur(T_ABR *racine,T_ABR *noeud);
T_ABR *Pere(T_ABR *racine,T_ABR *noeud);
void free_abr(T_ABR* abr);

//#endif // TP3_H_INCLUDED
