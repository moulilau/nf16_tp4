#include <stdio.h>
#include <stdlib.h>
#include "tp4.h"
#include <string.h>


void ajouterVaccinL(T_ListeVaccins** listeVaccins, char* marque, int nb_vaccins) {
    
    if (*listeVaccins == NULL) { //liste chainee est nulle : premier ajout
        T_ListeVaccins* nouveauVaccin = malloc(sizeof(T_ListeVaccins));   
        nouveauVaccin->marque = malloc(sizeof(marque) + 1);
        strcpy(nouveauVaccin->marque, marque);
        nouveauVaccin->nombre_vaccins = nb_vaccins;
        nouveauVaccin->suivant = NULL;
        *listeVaccins = nouveauVaccin; //creation du premier vaccin tete de liste
        return;
    }

    T_ListeVaccins* courante = *listeVaccins; //permet de se deplacer dans la liste chainee des vaccins
    T_ListeVaccins* precedente = NULL; //permet de conserver un pointeur sur l'element precedent

    while (courante != NULL) { 
        if (strcmp(marque, courante->marque) != 0) { //on verifie si le vaccin existe
            precedente = courante; //on se deplace dans la liste
            courante = courante->suivant;
        }
        else { //ou alors le vaccin existe et on incremente le nombre de vaccin
            courante->nombre_vaccins += nb_vaccins;
            return;
        }
    }
    //si la liste ne contient pas le vaccin on le creer a la fin de la liste
    T_ListeVaccins* nouveauVaccin = malloc(sizeof(T_ListeVaccins));
    precedente->suivant = nouveauVaccin; //on pense bien a faire pointer le dernier elmt non nul de la liste vers le nouvel elmt
    nouveauVaccin->marque = malloc(sizeof(marque) + 1);
    strcpy(nouveauVaccin->marque, marque);
    nouveauVaccin->nombre_vaccins = nb_vaccins;
    nouveauVaccin->suivant = NULL; 
}
void ajouterVaccinA(T_ABR** abr, char* date, char* marque, int nb_vaccins)
{    
    T_ABR* noeudCourant = *abr; //noeud courant recupere le premier noeud de l'arbre
    T_ABR* noeudPrecedent;

    if (noeudCourant == NULL) { //si l'arbre ne comprend pas de noeud
        *abr = creerElement(date); //creation de l'element
        noeudCourant = *abr;
        noeudCourant->liste_vaccins = NULL; //initialise la liste de vaccins a null
        T_ListeVaccins** ptListeElement = &noeudCourant->liste_vaccins; //creation d'un pointeur vers la liste de vaccin
        ajouterVaccinL(ptListeElement, marque, nb_vaccins); //appel de la fonction d'ajout d'un vaccin dans liste chainee
        printf("L'ajout a bien ete effectue.\n");
        return;
    }

    while (noeudCourant != NULL) {
        if (strcmp(date, noeudCourant->date) > 0) //si la date entree est superieur a celle de l'element observe on va vers la droite
        {
            noeudPrecedent = noeudCourant; //on  garde un acces au noeud precedent
            noeudCourant = noeudCourant->fils_droite; //et on avance dans l'arbre vers la droite
            if (noeudCourant == NULL) { //si ce nouveau noeud est null
                noeudPrecedent->fils_droite = creerElement(date); //on creer un nouvel element en le liant au precedent
                noeudPrecedent->fils_droite->liste_vaccins = NULL; //initialisation liste chainee a null
                T_ListeVaccins** ptListeVElement = &noeudPrecedent->fils_droite->liste_vaccins; //creation du pointeur
                ajouterVaccinL(ptListeVElement, marque, nb_vaccins); //appel fct ajout
                printf("L'ajout a bien ete effectue.\n");
                return;
            }
        }
        else if (strcmp(date, noeudCourant->date) == 0) { //si il y a egalite pas besoin de creer un element
            T_ListeVaccins** ptListeVElement = &noeudCourant->liste_vaccins; //on creer un pointeur
            ajouterVaccinL(ptListeVElement, marque, nb_vaccins); //on appel la fonction d'ajout
            printf("L'ajout a bien ete effectue.\n");
            return;
        }
        else { //si la date est inferieur a celle de l'element observe on va vers la gauche
            noeudPrecedent = noeudCourant; //on  garde un acces au noeud precedent
            noeudCourant = noeudCourant->fils_gauche;//et on avance dans l'arbre vers la gauche
            if (noeudCourant == NULL) { //si ce nouveau noeud est null
                noeudPrecedent->fils_gauche = creerElement(date); //on creer un nouvel element
                noeudPrecedent->fils_gauche->liste_vaccins = NULL; //on initialise la liste chainee a null
                T_ListeVaccins** ptListeVElement = &noeudPrecedent->fils_gauche->liste_vaccins; //on creer le pointeur
                ajouterVaccinL(ptListeVElement, marque, nb_vaccins); //appel fct ajout
                printf("L'ajout a bien ete effectue.\n");
                return;
            }
        }
    }
}

void afficherStockL(T_ListeVaccins* listeVaccins) {
    T_ListeVaccins* courante = listeVaccins;
    if (courante == NULL) {
        printf("Il n'y a aucun vaccin dans la liste");
        return;
    }
    printf("\tVaccin :\n");
    while (courante != NULL) {
        printf("\t ~ %s ~    Nombre de vaccins : %d\n", courante->marque, courante->nombre_vaccins);
        courante = courante->suivant;
    }
}

void afficherStockA(T_ABR* abr) {
    //on cherche a afficher les stock dans l'ordre croissant donc utilisation du parcours infixe
    if (abr != NULL) {
        afficherStockA(abr->fils_gauche); //On va d'abord tout à gauche de l'arbre
        printf("\n\t------ Date: %s ------\n", abr->date);
        afficherStockL(abr->liste_vaccins); //On affiche la liste des vaccins associée
        afficherStockA(abr->fils_droite); //On s'occupe ensuite de la partie droite de l'arbre
    }
}

int compterVaccins(T_ABR* abr, char* marque) {
    int nbVacc = 0; //variable qui permet de stocker le nombre de vaccin pour une date donnee
    if (abr != NULL) { //tant que l'arbre n'est pas null
        T_ListeVaccins* vaccinCourant = abr->liste_vaccins; //variable qui permet de se deplacer dans la liste chainee
        while (vaccinCourant != NULL) { //tant que la liste chainee n'est pas null
            if (strcmp(vaccinCourant->marque, marque) != 0) //si la marque ne correspond pas
                vaccinCourant = vaccinCourant->suivant; //passage a l'element suivant dans la liste
            else {
                nbVacc += vaccinCourant->nombre_vaccins; //sinon on recupere le nombre de vaccin
                break; //on sort de la boucle while car pour une date donnee il n' y aura qu' un seul element de la liste chainee qui portera
                        //le nom de la marque recherchee 
            }                    
        }
        return nbVacc + compterVaccins(abr->fils_gauche, marque) + compterVaccins(abr->fils_droite, marque); //on retourne le nb de vaccin de ce noeud 
                //ajouter a ceux de ses deux descendants
    }
    return 0;//si abr est nul on renvoie juste 0 car il n' y a pas de stock
}

void deduireVaccinL(T_ListeVaccins** listeVaccins, char*marque, int nb_vaccins) {
    T_ListeVaccins* courante = *listeVaccins;
    //T_ListeVaccins* pntCourante = courante;

    if (strcmp(marque, courante->marque) == 0) {//Cas ou le premier element de la liste est le bon (la bonne marque)
        courante->nombre_vaccins -= nb_vaccins;//Deduction des vaccins

        if (courante->nombre_vaccins <=0) { //Traitement du cas où nb_vaccins<=0 pour une marque donnée, ici suppression de la premiere marque de la liste
                //T_ListeVaccins* tmp = pntCourante->suivant;
                //free(courante);
                *listeVaccins = courante->suivant; //Suppression de la premiere marque
                free(courante);
           
        }
        return;
    }

    T_ListeVaccins* temp; //Permet de se deplacer dans la liste chainee des vaccins

    while (courante->suivant != NULL) {//Parcours de la liste pour trouver la bonne marque
        if (strcmp(courante->suivant->marque, marque) == 0) { //Element trouvé
            courante->suivant->nombre_vaccins -= nb_vaccins; //Deduction

            if (courante->suivant->nombre_vaccins <= 0) { //Traitement du cas où nb_vaccins <=0 pour une marque donnée
                temp = courante->suivant->suivant; //Supression de l'element avec un nombre de vaccin <= à 0
                free(courante->suivant);
                courante->suivant = temp;
                
            }
            return;
        } else {
            courante = courante->suivant; //On avance dans la liste
        }
    }
}

void deduireVaccinA(T_ABR** abr, char* marque, int nb_vaccins) {
    T_ABR* noeudCourant = *abr; //noeud courant recupere le premier noeud de l'arbre

    if (noeudCourant == NULL) { //si l'arbre ne comprend pas de noeud
        printf("Erreur il n'y a pas d'element dans l'arbre.");
        return;
    }

    while (noeudCourant->fils_gauche != NULL) { //sinon on cherche la plus petite date
        noeudCourant = noeudCourant->fils_gauche;
    }
    
    T_ListeVaccins* marqueCourante = noeudCourant->liste_vaccins; //pointeur pour se déplacer dans la liste chainée

    while (marqueCourante != NULL) { //tant que l'on est pas à la fin de la liste chainee
        T_ListeVaccins** ptListeVElement = &noeudCourant->liste_vaccins; //pointeur pour modifier la liste chainee
        if (strcmp(marqueCourante->marque, marque) != 0) { //tant qu'il n'y a pas d'egalite de la marque 
            marqueCourante = marqueCourante->suivant; //on se deplace dans la liste 
            if (marqueCourante == NULL) { //si on arrive à la fin sans avoir trouvé de match 
                noeudCourant = successeur(*abr, noeudCourant); //alors le noeud courant est le successeur 
                marqueCourante = noeudCourant->liste_vaccins; //et on va comparer le vaccin dans sa liste chainee
            }
        }
        else {
            int tmp = marqueCourante->nombre_vaccins - nb_vaccins; //on creer une variable temporaire pour regarder l'impact de la soustraction du nombre de vaccin
            if (tmp < 0) { //si il y a plus de vaccin a retirer que ceux presents dans la liste
                nb_vaccins -= marqueCourante->nombre_vaccins; //on diminue le nombre de vaccin total
                deduireVaccinL(ptListeVElement, marque, marqueCourante->nombre_vaccins); //et on enleve uniquement le nombre total de vaccin dans la liste pour cette marque
                //T_ABR* noeudtmp = successeur(*abr, noeudCourant);
                if (noeudCourant->liste_vaccins == NULL) { //si la liste de vaccin est null (alors il n'y a plus de stock pour cette date) et on supprime le noeud
                   char* dateSuppr = noeudCourant->date;
                   if (noeudCourant == *abr) { //si on veut supprimer la racine
                       *abr = supprimer_noeud(abr, dateSuppr);
                       noeudCourant = *abr;
                   }
                   else {
                       noeudCourant = successeur(*abr, noeudCourant);//On fait appel au successeur pour pouvoir passer au noeud suivant apres
                       *abr = supprimer_noeud(abr, dateSuppr);
                       } //on appel la suppression de cette date dans l'arbre en gardant son intergrite

                    marqueCourante = noeudCourant->liste_vaccins;
                }
                else { //si le noeud n'est pas null on passe au prochain noeud
                    noeudCourant = successeur(*abr, noeudCourant);
                    marqueCourante = noeudCourant->liste_vaccins;
                }
            }
            else { //s'il y a autant ou moins de vaccin
                deduireVaccinL(ptListeVElement, marque, nb_vaccins); //on soustrait le nombre de vaccin a la liste chainee
                if (noeudCourant->liste_vaccins == NULL) { //si la liste de vaccin est null (alors il n'y a plus de stock pour cette date) et on supprime le noeud
                    char* dateSuppr = noeudCourant->date;
                    *abr = supprimer_noeud(abr, dateSuppr); //on appel la suppression de cette date dans l'arbre en gardant son intergrite
                }
                return;
            }
        }
    }
}


/* ----------------------------------
 *   UTILITAIRES
 ---------------------------------- */
void viderBuffer() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

T_ABR* creerElement(char* date) {
    T_ABR* element = malloc(sizeof(T_ABR));
    element->date = date;
    element->fils_gauche = NULL;
    element->fils_droite = NULL;
    return element;
}


T_ABR* supprimer_noeud(T_ABR** abr, char* date) {
    T_ABR* abre = *abr;

    if (abre == NULL) { //si l'arbre est null il n'y a rien a faire on retourne null
        return NULL;
    }

    if (abre->fils_droite == NULL && abre->fils_gauche == NULL && abre->date == date){
        free_abr(abre);
        return NULL;
    }

    T_ABR* noeudCourant = abre; //noeud courant recupere le premier noeud de l'arbre, pour s'y deplacer

    // si date est inférieure à la valeur de la date du noeud
    if (strcmp(date, noeudCourant->date) < 0) {
        noeudCourant->fils_gauche = supprimer_noeud(&noeudCourant->fils_gauche, date); // rechercher dans le sous - arbre gauche
    }

        // si date est superieur à la valeur de la date du noeud
    else if (strcmp(date, noeudCourant->date) > 0) {
        noeudCourant->fils_droite = supprimer_noeud(&noeudCourant->fils_droite, date);//rechercher dans le sous - arbre droit
    }

        //sinon
    else {
        //le noeud courant a un fils unique
        if (noeudCourant->fils_gauche == NULL) {
            abre = noeudCourant->fils_droite;
            free(noeudCourant);
            return abre;
        }
        else if (noeudCourant->fils_droite == NULL) {
            abre = noeudCourant->fils_gauche;
            free(noeudCourant);
            return abre;
        }

        else {
            T_ABR* nvNoeud = noeudCourant->fils_droite;
            T_ABR* A_replacer = noeudCourant->fils_gauche;

            //Remplacer le noeud à supprimer par le fils droit
            noeudCourant->date = nvNoeud->date;
            noeudCourant->liste_vaccins = nvNoeud->liste_vaccins;
            noeudCourant->fils_gauche = nvNoeud->fils_gauche;
            noeudCourant->fils_droite = nvNoeud->fils_droite;

            //Une fois le bon noeud suprrimé on place le sous-arbre gauche de l'ancien noeud tout en bas à gauche du nouveau noeud
            while (noeudCourant->fils_gauche != NULL) { //On va à la plus petite date du sous-arbre
                noeudCourant = noeudCourant->fils_gauche;
            }
            noeudCourant->fils_gauche = A_replacer; //On place le sous-arbre qui était à replacer
        }

    }
    return *abr;
}

T_ABR* successeur(T_ABR *racine,T_ABR *noeud){ //Le successeur d'un noeud avec une certaine date sera le noeud avec la date superieur la plus proche
    T_ABR* successeur;

    if(noeud->fils_droite!= NULL) { //Si le sous-arbre droit du noeud n'est pas nul alors le successeur se trouve forcement dedans
        successeur = noeud->fils_droite;
        while (successeur->fils_gauche != NULL) { successeur = successeur->fils_gauche; } //On va chercher l'element le plus à gauche du sous arbre droit
    }
    else { //Si le sous-arbre droit du noeud est nul
        successeur = Pere(racine,noeud); // Recherche du pere pour pouvoir en déduire le successeur
        while (successeur != NULL && noeud == successeur->fils_droite){ // Tout pendant que le père n'est pas un noeud venant de droite dans l'arbe
            noeud = successeur;
            successeur = Pere(racine,noeud); //On remonte l'arbre
        }
    }
    return successeur;
}


T_ABR *Pere(T_ABR *racine,T_ABR *noeud){
    //Utilisation du parcours prefixe pour parcourir la liste, si on tombe sur le pere alors on retourne l'adresse du père
    T_ABR *courant= racine;
    //if (courant == noeud) {
    //    return NULL;
    //}
    if (courant!=NULL){ //Si on est pas à une extremité de l'arbre
        if (courant->fils_gauche==noeud || courant->fils_droite==noeud) //Si un des deux fils du courant est bien celui recherché
            return courant; //On a trouvé le père et on le renvoi
        else { //Si on a pas trouvé le pere
            T_ABR* P1 = Pere(racine->fils_gauche,noeud); //On réitère la fonction sur le sous arbre gauche
            T_ABR* P2 = Pere(racine->fils_droite,noeud); //On réitère la fonction sur le sous arbre droit
            if (P1 == NULL && P2 != NULL)
                return P2;
            if (P1 != NULL && P2 == NULL)
                return P1;
            else return NULL;
        }
    }
    else {return NULL;}
}

void free_abr(T_ABR* abr) {
    //parcours post-order pour désallouer la mémoire
    if (abr != NULL) { //tant que l'arbre n'est pas nul
        free_abr(abr->fils_droite);
        //on supprime les données de abr, pas besoin pour la date car elle n'est pas allouée dynamiquement
        T_ListeVaccins* tmp;
        T_ListeVaccins* tete = abr->liste_vaccins;
        while (tete != NULL) //tant que la liste chainee n'est pas nulle
        {
            tmp = tete;
            tete = tete->suivant;
            free(tmp);
        }
        free_abr(abr->fils_gauche);
        free(abr);
    }
}