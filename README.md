# NF16_TP4

<H1>Création d'une application en C permettant dé gérer des stockes de vaccins pour le covid. /
Creation of a C application allowing to manage stocks of vaccines for the covid.</H1>

<H2>Français:</H2>

L'appilaction permet:
- D'ajouter un stock de vaccins pour le covid d'une certaine marque à une certaine date dans un arbre binaire de recherche.
- D'afficher tous les stocks disponible 
- De compter le nombre de vaccin disponible pour une marque indiquée
- De déduire un nombre de vaccins d'une marque indiquée

L'utilisation des pointeurs est grandement utilitée dans ce projet. 


Projet numéro 4 réalisé au sein de l'UV NF16 à l'UTC par Cléa Bordeau et Laure Mouli.
Note: 17/20


<H2>English :</H2>

The application allows us:
- To add a stock of covid vaccines of a certain brand on a certain date in a binary search tree.
- To show all available stocks
- Count the number of vaccines available for an indicated brand
- To deduce a number of vaccines from an indicated brand

The use of pointers is greatly useful in this project.


Project number 4 carried out within the UV NF16 at UTC by Cléa Bordeau and Laure Mouli.
Grade: 17/20
